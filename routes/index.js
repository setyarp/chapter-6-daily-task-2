const router = require("express").Router();
// Controller
const Product = require("../controller/productController");
const Warehouse = require("../controller/warehouseController");
// const Auth = require('../controller/authController')
const Auth = require("./auth");
const warehouse = require("./warehouse");
const product = require("./product");

// middleware
// const uploader = require("../middlewares/uploader");
// const Authentication = require("../middlewares/authenticate");

// API server
router.use("/api/v1/products/", warehouse);

// API warehouse
router.use("/api/v1/warehouses/", warehouse);

// API auth
router.use("/api/v1/auth/", Auth);

module.exports = router;
