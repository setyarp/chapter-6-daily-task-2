const router = require("express").Router();

// controller
const Product = require("../controller/productController");

// middleware
const Authentication = require("../middlewares/authenticate");
const uploader = require("../middlewares/uploader");

// API product
router.post("/", Authentication, uploader.single("image"), Product.createProduct);
router.get("/", Product.findProducts);
router.get("/:id", Product.findProductById);
router.put("/:id", Product.updateProduct);
router.delete("/:id", Product.deleteProduct);

module.exports = router;
