const router = require("express").Router();

// controller
const Warehouse = require("../controller/warehouseController");

// middleware
// const Authentication = require('../middlewares/authenticate')

// API warehouse
router.get("/find", Warehouse.findWarehouses);
router.post("/create", Warehouse.createWarehouse);
router.get("/findbyid/:id", Warehouse.findWarehouseById);

module.exports = router;
